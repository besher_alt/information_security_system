﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace RSA_cryptography
{

    class myRSA
    {

        public static byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {

                    //Import the RSA Key information. This only needs
                    //to include the public key information.
                    RSA.ImportParameters(RSAKeyInfo);

                    //Encrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
                }
                return encryptedData;
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }

        }

        public static byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] decryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    //Import the RSA Key information. This needs
                    //to include the private key information.
                    RSA.ImportParameters(RSAKeyInfo);

                    //Decrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    decryptedData = RSA.Decrypt(DataToDecrypt, DoOAEPPadding);
                }
                return decryptedData;
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());

                return null;
            }

        }
    
    public static string EncryptFile(string path , RSACryptoServiceProvider rsa)
        {
            string file = path;
            byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            
           byte[] Encryptedbytes = RSAEncrypt(bytesToBeEncrypted, rsa.ExportParameters(false) , false );

            String Encryptedtext = Convert.ToBase64String(Encryptedbytes);
            return Encryptedtext;
        }

        public static string DecryptFile(string path, RSACryptoServiceProvider rsa)
        {

            String encryptedTXT = File.ReadAllText(path);

            byte[] bytesToBeDecrypted = Convert.FromBase64String(encryptedTXT);

            byte[] decryptedBytes = RSADecrypt(bytesToBeDecrypted, rsa.ExportParameters(true) , false);

            String decryptedtext = Encoding.UTF8.GetString(decryptedBytes);

            return decryptedtext;
        }

        //public static string DecryptFile(string path , byte[] passwordBytes)
        //{

        //    String encryptedTXT = File.ReadAllText(path);

        //    string decryptedtext  = Decrypt(encryptedTXT, passwordBytes);

        //    return decryptedtext;
        //}
        public static int GetSaltSize(byte[] passwordBytes)
        {
            var key = new Rfc2898DeriveBytes(passwordBytes, passwordBytes, 1000);
            byte[] ba = key.GetBytes(2);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ba.Length; i++)
            {
                sb.Append(Convert.ToInt32(ba[i]).ToString());
            }
            int saltSize = 0;
            string s = sb.ToString();
            foreach (char c in s)
            {
                int intc = Convert.ToInt32(c.ToString());
                saltSize = saltSize + intc;
            }

            return saltSize;
        }

        public static byte[] GetRandomBytes(int length)
        {
            byte[] ba = new byte[length];
            RNGCryptoServiceProvider.Create().GetBytes(ba);
            return ba;
        }
    }

}