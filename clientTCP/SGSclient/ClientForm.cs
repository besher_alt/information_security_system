using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using AES_cryptography;
using System.IO;
using System.Security.Cryptography;
using shared_Data;
using RSA_cryptography;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;

namespace Client
{
    //The commands for interaction between the server and the client
  

    public partial class Client : Form
    {
        private string fileIN = "Text.txt";
        private string fileOUT = "Text.enc.txt";
        private string xmlfile = "";

        public ClientInfo localInfo;
        public string strName;      //Name by which the user logs into the room
        static int bufferSize = 32000;
        private byte[] byteData = new byte[bufferSize];
        byte[] SignedHashValue;
        Data lastMsg;
        public Client()
        {
            InitializeComponent();
        }

        //Broadcast the message typed by the user to everyone
        //private void btnSend_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //Fill the info for the message to be send
        //        Data msgToSend = new Data();
                
        //        msgToSend.strName = strName;
        //        msgToSend.strMessage = txtMessage.Text;
        //        msgToSend.cmdCommand = Command.Message;

        //        byte [] byteData = msgToSend.ToByte();

        //        //Send it to the server
        //        localInfo.socket.BeginSend (byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnSend), null);

        //        txtMessage.Text = null;
        //    }
        //    catch (Exception)
        //    {
        //        MessageBox.Show("Unable to send message to the server.", "ClientTCP: " + strName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }  
        //}
        
        private void OnSend(IAsyncResult ar)
        {
            try
            {
                localInfo.socket.EndSend(ar);
            }
            catch (ObjectDisposedException)
            { }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ClientTCP: " + strName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnReceive(IAsyncResult ar)
        {
            try
            {
                localInfo.socket.EndReceive(ar);

                Data msgReceived = new Data(byteData);
                //Accordingly process the message received
                bool printmsg = true;
                switch (msgReceived.cmdCommand)
                {
                    case Command.Login:
                        printmsg = false;
                        string[] temp1 = msgReceived.strMessage.Split('#');
                        ClientInfo newClient = new ClientInfo(temp1[1]);
                        newClient.clientCertifcate = new X509Certificate2(msgReceived.certificate);
                       
                        txtChatBox.Text += newClient.strName + " has joined the room \r\n";


                        //certificate validation
                        X509Chain chain = new X509Chain();
                        chain.ChainPolicy = new X509ChainPolicy()
                        {
                            RevocationMode = X509RevocationMode.NoCheck,
                        };

                        try
                        {
                            var chainBuilt = chain.Build(newClient.clientCertifcate);
                            Console.WriteLine(string.Format("Chain building status: {0}", chainBuilt));

                            if (chainBuilt == false)
                            {
                                foreach (X509ChainStatus chainStatus in chain.ChainStatus)
                                    Console.WriteLine(string.Format("Chain error: {0} {1}", chainStatus.Status, chainStatus.StatusInformation));

                                txtChatBox.Text += newClient.strName + "'s certification is not valid \r\n";
                            }
                            else
                                txtChatBox.Text += newClient.strName + "'s certification is  valid \r\n";

                        }


                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }



                        lstChatters.Items.Add(newClient);

                        break;

                    case Command.Logout:
                        lstChatters.Items.Remove(msgReceived.strName);
                        break;

                    case Command.Message:
                        break;

                    case Command.AESencryptedFile:
                        printmsg = false;
                        File.WriteAllText(fileOUT, msgReceived.strMessage);
                        txtChatBox.Text += msgReceived.strName + " :: has sent encrypted file AES symmetric algorithm (enter the password and click decrypt to show it ) \r\n";
                        break;

                    case Command.RSAencryptedFile:
                        printmsg = false;
                        File.WriteAllText(fileOUT, msgReceived.strMessage);
                        lastMsg = msgReceived;
                        txtChatBox.Text += msgReceived.strName + " :: has sent encrypted file by RSA asymmetric algorithm (click decrypt to show it ) \r\n";
                        if (msgReceived.DigitalSign != null)
                        {
                            ClientInfo sender = getClientById(msgReceived.senderid);
                            msgInfo msginfo = new msgInfo(sender , msgReceived);
                            msginfo.WriteToXML(xmlfile);


                            txtChatBox.Text += msgReceived.strName + " :: has sent also digital signature  . \r\n";
                        }
                        break;

                    case Command.SessionKey:
                        printmsg = false;
                        RSACryptoServiceProvider rsa = GetKeyFromContainer();
                        byte[] encryptedSessionBytes = Convert.FromBase64String(msgReceived.strMessage);
                        byte[] decryptedSessionBytes = myRSA.RSADecrypt(encryptedSessionBytes, rsa.ExportParameters(true), false);
                        txtChatBox.Text += msgReceived.strName + " :: has sent new session key  by RSA asymmetric algorithm : \r\n ("
                                            + Encoding.UTF8.GetString(decryptedSessionBytes) + " ) \r\n ";
                        secureText.Text = Encoding.UTF8.GetString(decryptedSessionBytes);
                        break;


                    case Command.List:
                        printmsg = false;

                        var temp = msgReceived.strMessage.Split('*');
                        foreach(string data in temp)
                        {
                            if (data != "")
                            lstChatters.Items.Add(new ClientInfo(data) );
                        }

                        txtChatBox.Text += "<<<" + strName + " has joined the room>>>\r\n";
                        break;                    
                }

                if (msgReceived.strMessage != null &&  printmsg )
                    txtChatBox.Text += msgReceived.strMessage + "\r\n";

                byteData = new byte[bufferSize];

                localInfo.socket.BeginReceive(byteData,
                                          0, 
                                          byteData.Length,
                                          SocketFlags.None,
                                          new AsyncCallback(OnReceive),
                                          null);
                 
            }
            catch (ObjectDisposedException)
            { }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ClientTCP: " + strName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //get public / private keys from local windows files
    

        private void Form1_Load(object sender, EventArgs e)
        {


            fileOUT = strName + "enc.txt";
            xmlfile = "history\\" + strName + ".xml";
            this.Text = "ClientTCP: " + strName;
            
            //The user has logged into the system so we now request the server to send
            //the names of all users who are in the chat room
            Data msgToSend = new Data ();
            msgToSend.cmdCommand = Command.List;
            msgToSend.strName = strName;
            msgToSend.strMessage = null;
            
            byteData = msgToSend.ToByte();

            localInfo.socket.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnSend), null);
            
            byteData = new byte[bufferSize];
            //Start listening to the data asynchronously
            localInfo.socket.BeginReceive(byteData,
                                       0, 
                                       byteData.Length,
                                       SocketFlags.None,
                                       new AsyncCallback(OnReceive),
                                       null);

        }

        //private void txtMessage_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtMessage.Text.Length == 0)
        //        btnSend.Enabled = false;
        //    else
        //        btnSend.Enabled = true;
        //}

        private byte[] GetPasswordBytes()
        {
            // The real password characters is stored in System.SecureString
            // Below code demonstrates on converting System.SecureString into Byte[]
            // Credit: http://social.msdn.microsoft.com/Forums/vstudio/en-US/f6710354-32e3-4486-b866-e102bb495f86/converting-a-securestring-object-to-byte-array-in-net

            byte[] ba = null;

            if (secureText.Text.Length == 0)
                ba = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            else
            {
                // Convert System.SecureString to Pointer
                IntPtr unmanagedBytes = Marshal.SecureStringToGlobalAllocAnsi(secureText.SecureText);
                try
                {
                    // You have to mark your application to allow unsafe code
                    // Enable it at Project's Properties > Build
                    unsafe
                    {
                        byte* byteArray = (byte*)unmanagedBytes.ToPointer();

                        // Find the end of the string
                        byte* pEnd = byteArray;
                        while (*pEnd++ != 0) { }
                        // Length is effectively the difference here (note we're 1 past end) 
                        int length = (int)((pEnd - byteArray) - 1);

                        ba = new byte[length];

                        for (int i = 0; i < length; ++i)
                        {
                            // Work with data in byte array as necessary, via pointers, here
                            byte dataAtIndex = *(byteArray + i);
                            ba[i] = dataAtIndex;
                        }
                    }
                }
                finally
                {
                    // This will completely remove the data from memory
                    Marshal.ZeroFreeGlobalAllocAnsi(unmanagedBytes);
                }
            }

            return System.Security.Cryptography.SHA256.Create().ComputeHash(ba);
        }
        private void Client_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to leave the chat room?", "Client: " + strName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                //Send a message to logout of the server
                Data msgToSend = new Data ();
                msgToSend.cmdCommand = Command.Logout;
                msgToSend.strName = strName;
                msgToSend.strMessage = null;

                byte[] b = msgToSend.ToByte ();
                localInfo.socket.Send(b, 0, b.Length, SocketFlags.None);
                localInfo.socket.Close();
            }
            catch (ObjectDisposedException)
            { }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ClientTCP: " + strName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //private void txtMessage_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        btnSend_Click(sender, null);
        //    }
        //}


        private void secureText_TextChanged(object sender, EventArgs e)
        {

            if (secureText.Text.Length == 0)
            {
                encrypt.Enabled = false;
                decrypt.Enabled = false;
            }
            else
            {
                encrypt.Enabled = true;
                decrypt.Enabled = true;
            }
        }


        //aes encrypt file
        private void encrypt_Click(object sender, EventArgs e)
        {
            //take the password from secure input box
            byte[] passwordBytes = GetPasswordBytes();
            //encrypt the chosen file by the password
            String encryptedString = AES.EncryptFile(fileIN, passwordBytes);

            try
            {
                //Fill the info for the message to be send
                Data msgToSend = new Data();

                msgToSend.strName = strName;
                msgToSend.strMessage = encryptedString;
                msgToSend.cmdCommand = Command.AESencryptedFile;
                if (SignedHashValue != null)
                    msgToSend.DigitalSign = SignedHashValue;

                byte[] byteData = msgToSend.ToByte();

                //Send it to the server
                localInfo.socket.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnSend), null);

            }
            catch (Exception)
            {
                MessageBox.Show("Unable to send message to the server.", "ClientTCP: " + strName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void decrypt_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] passwordBytes = GetPasswordBytes();
                Stopwatch  AESStopwatch = new Stopwatch();
                AESStopwatch.Start();
                String decryptedString = AES.DecryptFile(fileOUT, passwordBytes);
                AESStopwatch.Stop();
                textBox1.Text = AESStopwatch.ElapsedMilliseconds.ToString() + "ms";


                txtChatBox.Text += decryptedString + " \r\n ";
            }
            catch (Exception ex)
            {
                txtChatBox.Text += "there is problem in decrypting the text make sure if your password is correct \r\n ";
            }
            
        }

        private void get_file_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text files | *.txt";
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                fileIN = dialog.FileName;
            }
        }

        private void sessionKeyGen_Click(object sender, EventArgs e)
        {   
            //generate AES session key
            AesCryptoServiceProvider crypto = new AesCryptoServiceProvider();

            crypto.KeySize = 256;
            crypto.GenerateKey();
            byte[] sessionkey = crypto.Key;
            txtChatBox.Text += "your generated session key is: \r\n (" + Encoding.UTF8.GetString(sessionkey) + " ) \r\n";
            secureText.Text = Encoding.UTF8.GetString(sessionkey);

            //send the generated key to the selected clients
            ListBox.SelectedObjectCollection selecteditem = lstChatters.SelectedItems;
            foreach (object item in selecteditem)
            {
                ClientInfo reciver = (ClientInfo)item;
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                //import recipient's public key 
                rsa.FromXmlString(reciver.KeyStringInfo);
                byte[] encryptedSessionKey = myRSA.RSAEncrypt(sessionkey, rsa.ExportParameters(false), false);
                string encryptedString = Convert.ToBase64String(encryptedSessionKey);

                try
                {
                    //Fill the info for the message to be send
                    Data msgToSend = new Data();

                    msgToSend.strName = strName;
                    msgToSend.strMessage = encryptedString;
                    msgToSend.cmdCommand = Command.SessionKey;//messege type is sessionkey
                    msgToSend.senderid = localInfo.id;//sendr id
                    msgToSend.receiverid = reciver.id;//eeciver id

                    byte[] byteData = msgToSend.ToByte();

                    //Send it to the server
                    localInfo.socket.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnSend), null);

                }
                catch (Exception)
                {
                    MessageBox.Show("Unable to send message to the server.", "ClientTCP: " + strName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
        }

        private void RSA_encrypt_Click(object sender, EventArgs e)
        {
            ListBox.SelectedObjectCollection selecteditem = lstChatters.SelectedItems;
            foreach (object item in selecteditem)
            {
                ClientInfo reciver = (ClientInfo)item;
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(reciver.KeyStringInfo);
                String encryptedString = myRSA.EncryptFile(fileIN, rsa);

                try
                {
                    //Fill the info for the message to be send
                    Data msgToSend = new Data();

                    msgToSend.strName = strName;
                    msgToSend.strMessage = encryptedString;
                    msgToSend.cmdCommand = Command.RSAencryptedFile;
                    msgToSend.senderid = localInfo.id;
                    msgToSend.receiverid = reciver.id;
                    if (SignedHashValue != null)
                        msgToSend.DigitalSign = SignedHashValue;
                    byte[] byteData = msgToSend.ToByte();

                    //Send it to the server
                    localInfo.socket.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnSend), null);

                }
                catch (Exception)
                {
                    MessageBox.Show("Unable to send message to the server.", "ClientTCP: " + strName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
        }
        //
        // Summary:
        //get local private key parameters
        public RSACryptoServiceProvider GetKeyFromContainer()        {
            // Create the CspParameters object and set the key container   
            // name used to store the RSA key pair.
            string containerName = localInfo.strName; 
            CspParameters cp = new CspParameters();
            cp.KeyContainerName = containerName;

            // Create a new instance of RSACryptoServiceProvider that accesses  
            // the key container MyKeyContainerName.  
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(cp);
            Console.WriteLine("Key retrieved from container : \n {0}", rsa.ToXmlString(true));

            return rsa;
            // Display the key information to the console.  
        }


       
        private void RSA_decrypt_Click(object sender, EventArgs e)
        {
            RSACryptoServiceProvider rsa = GetKeyFromContainer();

            Stopwatch RsaStopwatch = new Stopwatch();
            RsaStopwatch.Start();

            String decryptedString = myRSA.DecryptFile(fileOUT, rsa);
            RsaStopwatch.Stop();
            textBox2.Text = RsaStopwatch.ElapsedMilliseconds.ToString() + "ms";


            txtChatBox.Text += decryptedString + " \r\n ";

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void sign_btn_Click(object sender, EventArgs e)
        {

            string text = File.ReadAllText(fileIN);


            //Create a new instance of the UnicodeEncoding class to 
            //convert the string into an array of Unicode bytes.
            UnicodeEncoding UE = new UnicodeEncoding();

            //Convert the string into an array of bytes.
            byte[] MessageBytes = UE.GetBytes(text);

            //Create a new instance of the SHA1Managed class to create 
            //the hash value.
            SHA1Managed SHhash = new SHA1Managed();

            //Create the hash value from the array of bytes.
            byte[] HashValue = SHhash.ComputeHash(MessageBytes);

            //fetch private key infromation from windows key container 
            RSACryptoServiceProvider RSA = GetKeyFromContainer();

            //Create an RSAPKCS1SignatureFormatter object and pass it the   
            //RSACryptoServiceProvider to transfer the private key.  
            RSAPKCS1SignatureFormatter RSAFormatter = new RSAPKCS1SignatureFormatter(RSA);

            //Set the hash algorithm to SHA1.  
            RSAFormatter.SetHashAlgorithm("SHA1");

            //Create a signature for HashValue and assign it to   
            //SignedHashValue.  
            SignedHashValue = RSAFormatter.CreateSignature(HashValue);

            txtChatBox.Text += "the sign is ready \r\n";
        }

        private void verify_btn_Click(object sender, EventArgs e)
        {

            RSACryptoServiceProvider rsa = GetKeyFromContainer();
            String decryptedString = myRSA.DecryptFile(fileOUT, rsa);
            UnicodeEncoding UE = new UnicodeEncoding();

            //Convert the string into an array of bytes.
            byte[] MessageBytes = UE.GetBytes(decryptedString);
            //Create a new instance of the SHA1Managed class to create 
            //the hash value.
            SHA1Managed SHhash = new SHA1Managed();

            //Create the hash value from the array of bytes.
           byte[] HashValue = SHhash.ComputeHash(MessageBytes);

            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            string senderid = lastMsg.senderid;
            //get client info by his id
            ClientInfo DSsender = getClientById(senderid);
            // import public key
            RSA.FromXmlString(DSsender.KeyStringInfo);
            RSAPKCS1SignatureDeformatter RSADeformatter = new RSAPKCS1SignatureDeformatter(RSA);
            RSADeformatter.SetHashAlgorithm("SHA1");
            if (RSADeformatter.VerifySignature(HashValue, SignedHashValue))
            {
                Console.WriteLine("The signature is valid.");
                txtChatBox.Text += "The signature is valid. \r\n";
            }
            else
            {

                Console.WriteLine("The signature is not valid.");
                txtChatBox.Text += "The signature is not valid. \r\n";

            }
        }
        private ClientInfo getClientById(string id)
        {
            SignedHashValue = lastMsg.DigitalSign;
            ClientInfo DSsender = new ClientInfo();
            DSsender.id = id;
            if (lstChatters.Items.Contains(DSsender))
            {
                int index = lstChatters.Items.IndexOf(DSsender);
                DSsender = (ClientInfo)lstChatters.Items[index];
            }
            return DSsender;
        }
    }


    //The data structure by which the server and the client interact with 
    //each other
  
}