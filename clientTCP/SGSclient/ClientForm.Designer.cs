namespace Client
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Client));
            System.Security.SecureString secureString2 = new System.Security.SecureString();
            this.txtChatBox = new System.Windows.Forms.TextBox();
            this.lstChatters = new System.Windows.Forms.ListBox();
            this.encrypt = new System.Windows.Forms.Button();
            this.decrypt = new System.Windows.Forms.Button();
            this.get_file = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.secureText = new System.Windows.Forms.SecureTextBox();
            this.sessionKeyGen = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.RSA_encrypt = new System.Windows.Forms.Button();
            this.RSA_decrypt = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.sign_btn = new System.Windows.Forms.Button();
            this.verify_btn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtChatBox
            // 
            this.txtChatBox.BackColor = System.Drawing.SystemColors.Window;
            resources.ApplyResources(this.txtChatBox, "txtChatBox");
            this.txtChatBox.Name = "txtChatBox";
            this.txtChatBox.ReadOnly = true;
            // 
            // lstChatters
            // 
            this.lstChatters.BackColor = System.Drawing.SystemColors.ActiveCaption;
            resources.ApplyResources(this.lstChatters, "lstChatters");
            this.lstChatters.FormattingEnabled = true;
            this.lstChatters.Name = "lstChatters";
            this.lstChatters.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            // 
            // encrypt
            // 
            resources.ApplyResources(this.encrypt, "encrypt");
            this.encrypt.Name = "encrypt";
            this.encrypt.UseVisualStyleBackColor = true;
            this.encrypt.Click += new System.EventHandler(this.encrypt_Click);
            // 
            // decrypt
            // 
            resources.ApplyResources(this.decrypt, "decrypt");
            this.decrypt.Name = "decrypt";
            this.decrypt.UseVisualStyleBackColor = true;
            this.decrypt.Click += new System.EventHandler(this.decrypt_Click);
            // 
            // get_file
            // 
            resources.ApplyResources(this.get_file, "get_file");
            this.get_file.Name = "get_file";
            this.get_file.UseVisualStyleBackColor = true;
            this.get_file.Click += new System.EventHandler(this.get_file_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.secureText);
            this.groupBox1.Controls.Add(this.encrypt);
            this.groupBox1.Controls.Add(this.decrypt);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            // 
            // secureText
            // 
            this.secureText.AllowDrop = true;
            resources.ApplyResources(this.secureText, "secureText");
            this.secureText.Name = "secureText";
            this.secureText.SecureText = secureString2;
            this.secureText.UseSystemPasswordChar = true;
            this.secureText.TextChanged += new System.EventHandler(this.secureText_TextChanged);
            // 
            // sessionKeyGen
            // 
            resources.ApplyResources(this.sessionKeyGen, "sessionKeyGen");
            this.sessionKeyGen.Name = "sessionKeyGen";
            this.sessionKeyGen.UseVisualStyleBackColor = true;
            this.sessionKeyGen.Click += new System.EventHandler(this.sessionKeyGen_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.RSA_encrypt);
            this.groupBox2.Controls.Add(this.RSA_decrypt);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBox2
            // 
            resources.ApplyResources(this.textBox2, "textBox2");
            this.textBox2.Name = "textBox2";
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // RSA_encrypt
            // 
            resources.ApplyResources(this.RSA_encrypt, "RSA_encrypt");
            this.RSA_encrypt.Name = "RSA_encrypt";
            this.RSA_encrypt.UseVisualStyleBackColor = true;
            this.RSA_encrypt.Click += new System.EventHandler(this.RSA_encrypt_Click);
            // 
            // RSA_decrypt
            // 
            resources.ApplyResources(this.RSA_decrypt, "RSA_decrypt");
            this.RSA_decrypt.Name = "RSA_decrypt";
            this.RSA_decrypt.UseVisualStyleBackColor = true;
            this.RSA_decrypt.Click += new System.EventHandler(this.RSA_decrypt_Click);
            // 
            // sign_btn
            // 
            resources.ApplyResources(this.sign_btn, "sign_btn");
            this.sign_btn.Name = "sign_btn";
            this.sign_btn.UseVisualStyleBackColor = true;
            this.sign_btn.Click += new System.EventHandler(this.sign_btn_Click);
            // 
            // verify_btn
            // 
            resources.ApplyResources(this.verify_btn, "verify_btn");
            this.verify_btn.Name = "verify_btn";
            this.verify_btn.UseVisualStyleBackColor = true;
            this.verify_btn.Click += new System.EventHandler(this.verify_btn_Click);
            // 
            // Client
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.verify_btn);
            this.Controls.Add(this.sign_btn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.get_file);
            this.Controls.Add(this.sessionKeyGen);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lstChatters);
            this.Controls.Add(this.txtChatBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Client_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtChatBox;
        private System.Windows.Forms.ListBox lstChatters;
        private System.Windows.Forms.SecureTextBox secureText;
        private System.Windows.Forms.Button encrypt;
        private System.Windows.Forms.Button decrypt;
        private System.Windows.Forms.Button get_file;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button sessionKeyGen;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button RSA_encrypt;
        private System.Windows.Forms.Button RSA_decrypt;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button sign_btn;
        private System.Windows.Forms.Button verify_btn;
    }
}

