using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using shared_Data;
using System.Collections.Concurrent;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Asn1.X509;

namespace Server
{
    //The commands for interaction between the server and the client
  
    public partial class serverForm : Form
    {
        //The ClientInfo structure holds the required information about every
        //client connected to the server

        //The collection of all clients logged into the room (an array of type ClientInfo)

        ConcurrentDictionary<string, ClientInfo> clientList;
        //The main socket on which the server listens to the clients
        Socket serverSocket;

        static int bufferSize = 32000;

        byte[] byteData = new byte[32000];

        string issuerFileName = "CA.pfx"; // root certificate path

        X509Certificate2 issuerCertificate;

        public serverForm()
        {
            clientList =  new ConcurrentDictionary<string, ClientInfo>();
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {            
            try
            {
                //We are using TCP sockets
                serverSocket = new Socket(AddressFamily.InterNetwork, 
                                          SocketType.Stream, 
                                          ProtocolType.Tcp);

                //Assign the any IP of the machine and listen on port number 1000
                IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Loopback, 11500);

                //Bind and listen on the given address
                serverSocket.Bind(ipEndPoint);
                serverSocket.Listen(100);

                // load the root certificate

                 issuerCertificate = CAtools.LoadCertificate(issuerFileName, "password");

                //Accept the incoming clients
                serverSocket.BeginAccept(new AsyncCallback(OnAccept), null);
            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message, "SGSserverTCP",                     
                    MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }            
        }

        private void OnAccept(IAsyncResult ar)
        {
            try
            {
                Socket clientSocket = serverSocket.EndAccept(ar);

                //Start listening for more clients
                serverSocket.BeginAccept(new AsyncCallback(OnAccept), null);

                //Once the client connects then start receiving the commands from her
                clientSocket.BeginReceive(byteData, 0, byteData.Length, SocketFlags.None, 
                    new AsyncCallback(OnReceive), clientSocket);                
            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message, "SGSserverTCP", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
        }

        private void OnReceive(IAsyncResult ar)
        {
            try
            {
                Socket clientSocket = (Socket)ar.AsyncState;
                clientSocket.EndReceive(ar);

                //Transform the array of bytes received from the user into an
                //intelligent form of object Data
                Data msgReceived = new Data(byteData);

                //We will send this object in response the users request
                Data msgToSend = new Data();
                ClientInfo receiver;
                byte [] message;
                bool isBroadcast = false; // flag to broadcasted messages
                //If the message is to login, logout, or simple text message
                //then when send to others the type of the message remains the same
                msgToSend.cmdCommand = msgReceived.cmdCommand;
                msgToSend.strName = msgReceived.strName;
                msgToSend.senderid = msgReceived.senderid;
                msgToSend.DigitalSign = msgReceived.DigitalSign;

                switch (msgReceived.cmdCommand)
                {
                    case Command.Login:

                        isBroadcast = true;
                        //When a user logs in to the server then we add her to our
                        //list of clients

                        ClientInfo clientInfo = new ClientInfo(msgReceived.strMessage);
                        clientInfo.socket = clientSocket;
                        string SubjectName = "CN=" + clientInfo.strName;
                       X509Certificate2 subjectCertificate =  CAtools.IssueCertificate(SubjectName, clientInfo.KeyStringInfo , issuerCertificate, new[] { "server", "server.mydomain.com" }, new[] { KeyPurposeID.IdKPServerAuth });
                        clientInfo.clientCertifcate = subjectCertificate;
                        clientList.TryAdd(clientInfo.id, clientInfo);
          

                        //Set the text of the message that we will broadcast to all users
                        msgToSend.strMessage = "<<<" + msgReceived.strName + " has joined the room>>> \t\n #" + clientInfo.toMyString() ;
                        msgToSend.certificate = subjectCertificate.Export(X509ContentType.Pfx);
                        break;

                    case Command.Logout:
                        isBroadcast = true;
                        //When a user wants to log out of the server then we search for her 
                        //in the list of clients and close the corresponding connection

                        int nIndex = 0;
                        foreach (var client in clientList)
                        {
                            if (client.Value.socket == clientSocket)
                            {
                                ClientInfo temp;
                                clientList.TryRemove(client.Value.id, out temp );
                                break;
                            }
                            ++nIndex;
                        }
                        
                        clientSocket.Close();
                        
                        msgToSend.strMessage = "<<<" + msgReceived.strName + " has left the room>>>";
                        break;

                    case Command.Message:
                        isBroadcast = true;
                        //Set the text of the message that we will broadcast to all users
                        msgToSend.strMessage = msgReceived.strName + ": " + msgReceived.strMessage;
                        break;

                    case Command.AESencryptedFile:
                        isBroadcast = true;
                        //Set the text of the message that we will broadcast to all users
                        msgToSend.strMessage = msgReceived.strMessage;
                        break;

                    case Command.List:
                        isBroadcast = false;
                        //Send the names of all users in the chat room to the new user
                        msgToSend.cmdCommand = Command.List;
                        msgToSend.strName = null;
                        msgToSend.strMessage = null;

                        //Collect the names of the user in the chat room
                        foreach (var client in clientList)
                        {
                            //To keep things simple we use asterisk as the marker to separate the user names
                            msgToSend.strMessage += client.Value.toMyString() + "*";   
                        }

                        message = msgToSend.ToByte();

                        //Send the name of the users in the chat room
                        clientSocket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                new AsyncCallback(OnSend), clientSocket);                        
                        break;

                    case Command.RSAencryptedFile:
                        isBroadcast = false;

                        
                        clientList.TryGetValue(msgReceived.receiverid, out receiver);
                        msgToSend.strMessage = msgReceived.strMessage;
                        message = msgToSend.ToByte();
                        receiver.socket.BeginSend(message, 0, message.Length, SocketFlags.None,
                    new AsyncCallback(OnSend), receiver.socket);
                        break;

                    case Command.SessionKey:
                        isBroadcast = false;

                        
                        clientList.TryGetValue(msgReceived.receiverid, out receiver);
                        msgToSend.strMessage = msgReceived.strMessage;
                        message = msgToSend.ToByte();
                        receiver.socket.BeginSend(message, 0, message.Length, SocketFlags.None,
                    new AsyncCallback(OnSend), receiver.socket);
                        break;
                }

                if (isBroadcast)   // send broadcasted messsages to all
                {
                    message = msgToSend.ToByte();

                    foreach (var clientInfo in clientList)
                    {
                        if (clientInfo.Value.socket != clientSocket ||
                             msgToSend.cmdCommand != Command.Login)
                        {
                            //Send the message to all users
                            clientInfo.Value.socket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                new AsyncCallback(OnSend), clientInfo.Value.socket);                            
                        }
                    }
                    if (txtLog.InvokeRequired)
                    {
                        txtLog.Invoke(new MethodInvoker(delegate { txtLog.Text += msgToSend.strMessage + "\r\n"; }));
                    }
                }

                //If the user is logging out then we need not listen from her
                if (msgReceived.cmdCommand != Command.Logout)
                {
                    //Start listening to the message send by the user
                    clientSocket.BeginReceive(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnReceive), clientSocket);
                }
            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message, "SGSserverTCP", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
        }

        public void OnSend(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                client.EndSend(ar);                
            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message, "SGSserverTCP", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
        }
    }

    //The data structure by which the server and the client interact with 
    //each other

}