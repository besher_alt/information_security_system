﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace shared_Data
{
    public  class msgInfo
    {
        public string senderName;
        public string Msg;
        public byte[] digitalSignature;
        public string publicKey;
        public DateTime receivedDate;
    public  msgInfo(ClientInfo clientinfo , Data data)
    {
            senderName = clientinfo.strName;
            Msg = data.strMessage;
            digitalSignature = data.DigitalSign;
            publicKey = clientinfo.KeyStringInfo;
            receivedDate = DateTime.Now;
    }

        public msgInfo()
        {

        }

        public  void WriteToXML(string path)
        {
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(msgInfo));

            System.IO.FileStream file = new System.IO.FileStream(path, FileMode.Append, FileAccess.Write);

            writer.Serialize(file, this);
            file.Close();
        }

    }

    
}
