﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace shared_Data
{
    public class ClientInfo
    {
        public Socket socket;   //Socket of the client
        public string strName;  //Name by which the user logged into the chat room
        public string KeyStringInfo;
        public string id;
        public X509Certificate2 clientCertifcate;
        public void generateID()
        {
            id = Guid.NewGuid().ToString("N");
        }

        public override string ToString()
        {
            return strName;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;
            ClientInfo temp = (ClientInfo)obj;
            if (temp.id == this.id)
                return true;
            else
                return false;
        }

        public string toMyString()
        {
            return id + "@" + strName + "@" + KeyStringInfo;

        }
        public ClientInfo()
        {

        }
        public ClientInfo(string data)
        {
            
                var temp = data.Split('@');
                this.id = temp[0];
                this.strName = temp[1];
                this.KeyStringInfo = temp[2];
            
        }

        public ClientInfo(byte[] data)
        {
            

            //The next four store the length of the name
            int nameLen = BitConverter.ToInt32(data, 4);

            //The next four store the length of the message
            int msgLen = BitConverter.ToInt32(data, 8);

            //This check makes sure that strName has been passed in the array of bytes
            if (nameLen > 0)
                this.strName = Encoding.UTF8.GetString(data, 12, nameLen);
            else
                this.strName = null;

            //This checks for a null message field
            if (msgLen > 0)
                this.KeyStringInfo = Encoding.UTF8.GetString(data, 12 + nameLen, msgLen);
            else
                this.KeyStringInfo = null;
        }


        public byte[] ToByte()
        {
            List<byte> result = new List<byte>();
 

            //Add the length of the name
            if (strName != null)
                result.AddRange(BitConverter.GetBytes(strName.Length));
            else
                result.AddRange(BitConverter.GetBytes(0));

            //Length of the message
            if (KeyStringInfo != null)
                result.AddRange(BitConverter.GetBytes(KeyStringInfo.Length));
            else
                result.AddRange(BitConverter.GetBytes(0));

            //Add the name
            if (strName != null)
                result.AddRange(Encoding.UTF8.GetBytes(strName));

            //And, lastly we add the message text to our array of bytes
            if (KeyStringInfo != null)
                result.AddRange(Encoding.UTF8.GetBytes(KeyStringInfo));

            return result.ToArray();
        }
    }

    public enum Command
    {
        Login,      //Log into the server
        Logout,     //Logout of the server
        AESencryptedFile, // send encrypted File by AES cryptocraphy
        Message,    //Send a text message to all the chat clients
        List,       //Get a list of users in the chat room from the server
        Null,        //No command
        RSAencryptedFile,  // send encrypted File by RSA cryptocraphy 
        SessionKey          // send session key
    }

    public class Data
    {
        //Default constructor
        public Data()
        {
            this.cmdCommand = Command.Null;
            this.strMessage = null;
            this.strName = null;
            this.senderid = null;
            this.receiverid = null;
            
            
            
        }

        //Converts the bytes into an object of type Data
        public Data(byte[] data)
        {
            //The first four bytes are for the Command
            this.cmdCommand = (Command)BitConverter.ToInt32(data, 0);

            //The next four store the length of the name
            int nameLen = BitConverter.ToInt32(data, 4);

            //The next four store the length of the message
            int msgLen = BitConverter.ToInt32(data, 8);

            int senderLen = BitConverter.ToInt32(data, 12);
            //The next four store the length of the receiver name
            int receiverLen = BitConverter.ToInt32(data, 16);

            //The next four store the length of the digital sign
            int DSlen = BitConverter.ToInt32(data, 20);

            //The next four store the length of the certificate
            int crlen = BitConverter.ToInt32(data, 24);

            int length = 28;
            //This check makes sure that strName has been passed in the array of bytes
            if (nameLen > 0)
                this.strName = Encoding.UTF8.GetString(data, length, nameLen);
            else
                this.strName = null;

            length += nameLen;
            //This checks for a null message field
            if (msgLen > 0)
                this.strMessage = Encoding.UTF8.GetString(data, length, msgLen);
            else
                this.strMessage = null;

            length += msgLen;

            if (senderLen > 0)
                this.senderid = Encoding.UTF8.GetString(data, length, senderLen);
            else
                this.senderid = null;

            length += senderLen;
            if (receiverLen > 0)
                this.receiverid = Encoding.UTF8.GetString(data, length , receiverLen);
            else
                this.receiverid = null;

            length += receiverLen;
            if (DSlen > 0)
                this.DigitalSign = data.Skip(length).Take(DSlen).ToArray();
            else
                this.DigitalSign = null;

            length += DSlen;
            if (crlen> 0)
                this.certificate= data.Skip(length).Take(crlen).ToArray();
            else
                this.certificate = null;
        }

        //Converts the Data structure into an array of bytes
        public byte[] ToByte()
        {
            byte[] certificateBytes = null;
            List<byte> result = new List<byte>();

            //First four are for the Command
            result.AddRange(BitConverter.GetBytes((int)cmdCommand));

            //Add the length of the name
            if (strName != null)
                result.AddRange(BitConverter.GetBytes(strName.Length));
            else
                result.AddRange(BitConverter.GetBytes(0));

            //Length of the message
            if (strMessage != null)
                result.AddRange(BitConverter.GetBytes(strMessage.Length));
            else
                result.AddRange(BitConverter.GetBytes(0));

            //Add the length of the name
            if (senderid != null)
                result.AddRange(BitConverter.GetBytes(senderid.Length));
            else
                result.AddRange(BitConverter.GetBytes(0));

            //Add the length of the name
            if (receiverid != null)
                result.AddRange(BitConverter.GetBytes(receiverid.Length));
            else
                result.AddRange(BitConverter.GetBytes(0));

            if (DigitalSign != null)
                result.AddRange(BitConverter.GetBytes(DigitalSign.Length));
            else
                result.AddRange(BitConverter.GetBytes(0));
            if(certificate!= null)
            {
                certificateBytes = certificate;
                result.AddRange(BitConverter.GetBytes(certificateBytes.Length));
            }
            else
                result.AddRange(BitConverter.GetBytes(0));


            //Add the name
            if (strName != null)
                result.AddRange(Encoding.UTF8.GetBytes(strName));

            //And, lastly we add the message text to our array of bytes
            if (strMessage != null)
                result.AddRange(Encoding.UTF8.GetBytes(strMessage));

            if (senderid != null)
                result.AddRange(Encoding.UTF8.GetBytes(senderid));

            if (receiverid != null)
                result.AddRange(Encoding.UTF8.GetBytes(receiverid));

            if (DigitalSign != null)
                result.AddRange(DigitalSign);

            if (certificateBytes != null)
                result.AddRange(certificateBytes);


            return result.ToArray();
        }

        public string strName;      //Name by which the client logs into the room
        public string strMessage;   //Message text
        public Command cmdCommand;  //Command type (login, logout, send message, etcetera)
        public string senderid;     //  sender id
        public string receiverid;   // receiver id
        public byte[] DigitalSign;  // digital sign if is available
        public byte[] certificate;  // certificate  if is available
    }
}
